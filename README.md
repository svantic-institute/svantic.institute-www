# Website http://svantic.institute

In diesem Git-Repository befindet sich die Website vom Svantic Institute.

Das primäre Git-Repository im Web dazu lautet https://bitbucket.org/svantic-institute/svantic.institute-www/

## Hosting/Deploying

Die Domain http://svantic.institute wird auf dem Server kolja.ufopixel.de
von Sven in ngnix gehostet.

Bei Bitbucket gibt es einen Webhook beim Push
zu https://svenk.org/svantic-institute-updater/update.php (gleicher Server).
Da wird dann einfach ein git clone ausgeführt, und zwar mithilfe eines SSH
Read-Only Access Keys
(https://bitbucket.org/svantic-institute/svantic.institute-www/admin/access-keys/).

Kurzum: Git pushen, dann wird veröffentlicht.


